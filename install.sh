#!/usr/bin/env bash

function link_file {
    source="${PWD}/$1"
    target="${HOME}/${1/_/.}"

    if [ -e "${target}" ] && [ ! -L "${target}" ]; then
        mv $target $target.bak
    fi

    ln -sfn ${source} ${target}
}

for i in _*
do
    link_file $i
done

# delete all broken symlinks
find -L ~ -maxdepth 1 -type l -delete

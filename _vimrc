" ----------------------------------------------------------------------------
" file:     ~/.vimrc
" author:   Daniel Henschel - http://is-bored.de/
" modified: May 2013
" vim:      si:et:ts=4:sw=4:ft=vim
" ----------------------------------------------------------------------------

" shortcuts
set nocompatible                    " don't be conpatible to vi
let mapleader=","                   " change the leader to ,

" basics ---------------------------------------------------------------------
syntax on                   " syntax highlighting
filetype on                 " enable filetype detection
filetype plugin indent on   " enable loading indent file for filetype
set number                  " show line numbers
set numberwidth=1           " as short as possible
set background=dark         " use dark background
set title                   " show title in console bar
colorscheme solarized       " solarized colors

" completion
set wildmenu                " menu completion in command mode on <Tab>
set wildmode=full           " <Tab> cycles between all choices
set wildignore+=*.o,*.obj,.git,*.pyc    " ignore these when completing
set completeopt=menuone,longest,preview " don't select first item
set pumheight=6                         " small completion window

" files
set directory=~/.vim/tmp        " put .swp here
set viminfo=""                  " disable ~/.viminfo
set encoding=utf-8              " use unicode
set fileformats=unix,dos,mac    " use unix format by default

" moving / editing
set cursorline          " highlight current line
set ruler               " show the cursor position all the time
set scrolloff=3         " keep 3 lines of context
set autoindent          " always set autoindenting on
set smartindent         " use smart indent if ther is no file
set tabstop=4           " <tab> = 4 spaces
set shiftwidth=4        " indent level = 4 spaces
set softtabstop=4       " backspace fake tabs
set expandtab           " use spaces instead of tabs
set smarttab            " smart tabbing
set foldmethod=indent   " allow to fold on indents
set foldlevel=99        " don't fold by default
set textwidth=78        " normal width
set colorcolumn=+1      " highlight linebreak
set mouse=a             " use mouse

if has('autocmd')
    " close preview when moving
    autocmd CursorMovedI * if pumvisible() == 0|pclose|endif
    autocmd InsertLeave * if pumvisible() == 0|pclose|endif
endif

" reading / writing
set noautowrite         " never write a file unless requested
set noautowriteall      " NEVER.
set modeline            " allow vim options to be embedded in files
set modelines=5         " have to be in the first or last 5 lines

" messages / info / status
set laststatus=2        " always show status line
set showcmd             " show incomplete normal mode commands as I type
set statusline=%-3.3n\ %f\ %h%m%r%w
set statusline+=[%{strlen(&filetype)?&filetype:'txt'},%{&encoding},%{&fileformat}]
set statusline+=%=0x%-8B\ %-10.(%l,%c%V%)

" searching and patterns
set ignorecase          " use case insensitive searches
set smartcase           " unless I use uppercase
set hlsearch            " highlight results
set incsearch           " search as I type
highlight Search cterm=underline ctermbg=none gui=underline guibg=none

" hacks ----------------------------------------------------------------------
if has('autocmd')
    " highlight trailing chars
    autocmd BufWinEnter * match ExtraWhitespace /\s\+\%#\@<!$/
    highlight ExtraWhitespace ctermbg=red guibg=red

    " reload .vimrc if edited
    autocmd! BufWritePost .vimrc source ~/.vimrc
endif
